package assign2;
import java.util.*;
public class Employee {
	int id,age,salary;
	String name,department,city;
	public Employee(int id, String name, int age, int salary, String department, String city) throws IllegalArgumentException {
		super();
		if(id<0) {
			throw new IllegalArgumentException("exception occured, id cannot less than zero");
		}
		this.id = id;
		if(name==null) {
			throw new IllegalArgumentException("exception occured, name cannot be null");
		}
		this.name = name;
		if(age<=0) {
			throw new IllegalArgumentException("exception occured, age cannot be zero");
		}
		this.age = age;
		if(salary<0) {
			throw new IllegalArgumentException("exception occured, salary cannot be zero");
		}
		this.salary = salary;
		if( department == null) {
			throw new IllegalArgumentException("exception occured, department cannot be null");
		}
		this.department = department;
		if(city==null) {
			throw new IllegalArgumentException("exception occured, city cannot be null");
		}
		this.city = city;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
    }
	public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    @Override
    public String toString() {
    	return "\n|  " + id + "  | " + name + " | " + age + " | " + salary + " " + department + "    | " + city + "|";
    }

}