package assign2;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;
public class DataStructureB{
	public void cityNameCount(ArrayList<Employee> employees) {
		System.out.println("\nCount of Employees from each city:\n");
		Set<String> s = new TreeSet<String>();
        ArrayList<String> a1 = new ArrayList<String>();
        for (Employee e : employees) {
        	a1.add(e.getCity());
        	s.add(e.getCity());
        }
        for (String e : s) {
        	System.out.print(e + "=" + Collections.frequency(a1, e) + ",   ");
        }
}
public void monthlySalary(ArrayList<Employee> employees) {
	System.out.println("\n\nMonthly Salary of employee along with their ID is:\n");
	for (Employee e1 : employees) {
		System.out.print(e1.getId() + "=" + e1.getSalary() / 12.0 + ",   ");
	}
}
}
